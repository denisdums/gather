<?php

namespace App\View\Components\Buttons;

use Illuminate\View\Component;

class Menu extends Component
{
    /**
     * The route.
     *
     * @string Route
     */
    public $route;

    /**
     * The title.
     *
     * @string Title
     */
    public $title;

    /**
     * The icon name.
     *
     * @string Icon
     */
    public $icon;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($route, $title, $icon)
    {
        $this->route = $route;
        $this->title = $title;
        $this->icon = $icon;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.buttons.menu');
    }
}
