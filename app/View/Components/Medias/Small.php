<?php

namespace App\View\Components\Medias;

use Illuminate\View\Component;

class Small extends Component
{

    /**
     * The media.
     *
     * @object media
     */
    public $media;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($media)
    {
        $this->media = $media;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.medias.small');
    }
}
