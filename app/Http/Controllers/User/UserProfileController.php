<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserProfileController extends Controller
{
    public function render($slug)
    {
        $current_user = User::where('slug', $slug)->first();
        if ($current_user == null) {
            return abort(404);
        }
        $user_medias = Media::where('authorID', $current_user->id)->orderBy('created_at', 'DESC')->get()->all();

        $data = [
            'user' => $current_user,
            'user_medias' => $user_medias,
        ];

        return view('user.profile', $data);
    }

    public function relationship()
    {
        $userID = request('user_id');
        $user = User::findOrFail($userID);
        auth()->user()->follows()->toggle($userID);

        $results = [
            'following' => $user->following(),
            'followers' => count($user->followers)
        ];

        return json_encode($results);
    }

    public function subscriptions()
    {
        $follows = auth()->user()->follows;
        $followsID = [];

        foreach ($follows as $follow){
            $followsID [] = $follow->id;
        }

        $medias = Media::whereIn('authorID', $followsID)->get()->all();

        $data = [
            'subs' => $medias,
        ];

        return view('user.subscriptions', $data);
    }

    public function render_edit(){
        $data = [
            'user' => auth()->user(),
        ];
        return view('user.edit', $data);
    }

    public function edit(){
        $user = auth()->user();
        /*
         * Create unique name for the avatar file and upload it
         */
        if (request('avatar')){
            $image_name = time() . '.' . request('avatar')->extension();
            request('avatar')->move(public_path('uploads/user-avatar'), $image_name);
            $user->avatar = $image_name;
        }
        /*
         * Create the slug of the user from his name
         */
        $slug = preg_replace('~[^\pL\d]+~u', '-', request('name'));
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        $slug = preg_replace('~[^-\w]+~', '', $slug);
        $slug = trim($slug, '-');
        $slug = preg_replace('~-+~', '-', $slug);
        $slug = strtolower($slug);

        $user->slug = $slug;
        $user->name = request('name');
        $user->email = request('email');
        $user->save();

        $data = [
            'user' => $user,
        ];

        return view('user.edit', $data);
    }
}
