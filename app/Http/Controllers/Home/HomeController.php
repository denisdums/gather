<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function render()
    {
        $medias = Media::orderBy('created_at', 'DESC')->get()->all();
        $data = [
            'medias' => $medias,
        ];

        return view('home', $data);
    }
}
