<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search()
    {
        $search = request('search');
        $results = Media::select('name','slug')->where('name','LIKE','%'.$search.'%')->get()->unique('name');
        return json_encode($results);
    }
}
