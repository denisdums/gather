<?php

namespace App\Http\Controllers\Medias;

use App\Http\Controllers\Controller;
use App\Models\Media;
use App\Models\User;
use Illuminate\Http\Request;

class WatchMediaController extends Controller
{
    public function watch($slug)
    {
        $main_media = Media::where('slug', $slug)->first();
        if (! $main_media){
            return abort('404');
        }
        $main_media->update(['deleted' => $main_media->views++]);
        $other_medias = Media::where('authorID', $main_media->user->id)->where('id','!=',$main_media->id)->orderBy('created_at', 'DESC')->get()->all();
        $data = [
            'user' => $main_media->user,
            'main_media' => $main_media,
            'other_medias' => $other_medias,
        ];

        return view('medias.watch', $data);
    }
}
