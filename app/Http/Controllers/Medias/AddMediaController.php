<?php

namespace App\Http\Controllers\Medias;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;


class AddMediaController extends Controller
{
    public function render($user)
    {
        if ( $user != auth()->user()->slug){
            return abort(404);
        }else {
            return view('medias.add');
        }

    }

    public function delete()
    {
        $mediaID = request('media_id');
        $media = Media::where('id', $mediaID)->first();
        $media->delete();
        return true;
    }

    public function add()
    {
        /*
         * Check the content of the form
         */
        request()->validate([
            'media-file' => ['required','mimes:h264,mp4,mpeg'],
            'media-name' => ['required'],
            'media-description' => ['required']
        ]);

        /*
         * Uploads the video
         */
        $media_file_name = time();
        $media_video_name = $media_file_name.'.'.request('media-file')->extension();
        request('media-file')->move(public_path('uploads/medias'), $media_video_name);

        /*
         * Get the thumbnail of the video
         */
        $video_thumbnail_name = $media_file_name.'.png';
        FFMpeg::fromDisk('medias')
            ->open($media_video_name)
            ->getFrameFromSeconds(1)
            ->export()
            ->toDisk('thumbnails')
            ->save($video_thumbnail_name);

        /*
         * Get the duration of the video
         */
        $media =  FFMpeg::fromDisk('medias')->open($media_video_name);
        $duration = $media->getDurationInSeconds();

        /*
         * Create unique slug of the video from the title and the file name
         */
        $slug = request('media-name').'-'.$media_file_name;
        $slug = preg_replace('~[^\pL\d]+~u', '-', $slug);
        $slug = iconv('utf-8', 'us-ascii//TRANSLIT', $slug);
        $slug = preg_replace('~[^-\w]+~', '', $slug);
        $slug = trim($slug, '-');
        $slug = preg_replace('~-+~', '-', $slug);
        $slug = strtolower($slug);

        /*
         * Create and save the media
         */
        Media::create([
            'views' => '0',
            'duration' => $duration,
            'slug' => $slug,
            'thumbnail' => $video_thumbnail_name,
            'filename' => $media_video_name,
            'name' => request('media-name'),
            'description' => request('media-description'),
            'authorID' => auth()->user()->id,
        ]);

        /*
         * Redirect the user
         */
        return redirect('/user/'.auth()->user()->slug);
    }

    public function render_edit($id)
    {
        $media = Media::where('id', $id)->first();
        return view('medias.edit',['media'=>$media]);
    }

    public function edit($id)
    {
        $media = Media::where('id', $id)->first();

        $media->name = request('media-name');
        $media->description = request('media-description');
        $media->save();

        return view('medias.edit',['media'=>$media]);
    }
}
