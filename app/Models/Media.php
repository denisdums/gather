<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Media extends Model
{
    public $table = 'medias';
    protected $fillable = [
        'media-file',
        'name',
        'description',
        'thumbnail',
        'filename',
        'authorID',
        'slug',
        'duration',
        'views'
    ];


    public function get_media_url()
    {
        return asset('uploads/medias/' . $this->filename);
    }

    public function get_media_thumbnail()
    {
        return asset('uploads/thumbnails/' . $this->thumbnail);
    }

    public function get_small_title()
    {
        $name= $this->name;
        $smallTitle = substr($name,0,30);
        if (strlen($name) > 30){
            $smallTitle = $smallTitle.'...';
        }
        return $smallTitle;
    }

    public function get_media_author()
    {
        return User::where('id', $this->authorID)->first()->name;
    }

    public function link()
    {
        return '/watch/' . $this->slug;
    }

    public function views()
    {
        $views = $this->views;
        if ($views <= 1){
            return $views.' vue';
        } else return  $views.' vues';
    }

    public function posted()
    {
        $time = $this->created_at;
        $time = strtotime($time);
        $diff_time = time() - $time;

        if ($diff_time < 1) {
            return "A l'instant";
        }

        $sec = array(
            31556926 => 'an',
            2629743.83 => 'mois',
            86400 => 'jour',
            3600 => 'heure',
            60 => 'minute',
            1 => 'seconde'
        );
        foreach ($sec as $sec => $value){
            $div = $diff_time / $sec;
            if ($div >= 1){
                $time_ago = round($div);
                $time_type = $value;
                if ($div > 1){
                    $time_type = $value.'s';
                }
                return "Il y a $time_ago $time_type";
            }
        }
    }

    public function time()
    {
        $duration = $this->duration;
        if ($duration < 60){
            $duration = '00:'.$duration;
        } else {
            $duration = gmdate("H:i:s", $duration);
            if (substr($duration,'0','2') == '00'){
                $duration = substr($duration,'3','5');
            }
        }
        return $duration;
    }

    public function user() {
        return $this->belongsTo('App\Models\User','authorID');
    }
}
