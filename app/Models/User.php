<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'avatar',
        'slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function avatar()
    {
        return asset('uploads/user-avatar/' .$this->avatar);
    }

    public function profil()
    {
        return '/user/'.$this->slug;
    }

    public function follows(){
        return $this->belongsToMany('App\Models\User','connexion','from','to');
    }

    public function followers(){
        return $this->belongsToMany('App\Models\User','connexion','to','from');
    }

    public function following(){
       return auth()->user()->follows->contains($this->id);
    }


}
