<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Medias\AddMediaController;
use App\Http\Controllers\Medias\WatchMediaController;
use App\Http\Controllers\User\UserProfileController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Search\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
 * Protected routes for auth users
 */
Auth::routes();

Route::group([
    'middleware' => 'auth',
], function(){
    Route::get('/{user}/add', [AddMediaController::class, 'render']);
    Route::post('/{user}/add', [AddMediaController::class, 'add']);
    Route::post('/relationship', [UserProfileController::class, 'relationship']);
    Route::get('/subscriptions', [UserProfileController::class, 'subscriptions'])->name('subscriptions');
    Route::post('/media-delete', [AddMediaController::class, 'delete']);
    Route::get('/edit/media/{id}', [AddMediaController::class, 'render_edit']);
    Route::post('/edit/media/{id}', [AddMediaController::class, 'edit']);
    Route::get('/user/edit/', [UserProfileController::class, 'render_edit']);
    Route::post('/user/edit/', [UserProfileController::class, 'edit']);
});


/*
 * Routes open for all users
 */
Route::get('/', [HomeController::class, 'render'])->name('home');
Route::get('/watch/{slug}', [WatchMediaController::class, 'watch']);
Route::get('/user/{user}', [UserProfileController::class, 'render']);
Route::post('/search', [SearchController::class, 'search']);

/*
 * Redirect to 404 if errors
 */
Route::fallback(function () {
    abort('404');
});
