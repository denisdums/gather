@extends('layouts.app')
@section('title','Home')
@section('content')
    <div class="container">
        @if( count($medias) > 0)
            <h2 class="gather-title">Vidéos les plus récentes</h2>
            <div class="gather-small-medias">
                @foreach($medias as $media)
                    <x-medias.small :media="$media"></x-medias.small>
                @endforeach
            </div>
        @else
            <p class="gather-nothing">Oh, on dirait qu'il n'y a pas encore de vidéos 😞</p>
        @endif
    </div>
@endsection
