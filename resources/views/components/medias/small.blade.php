<a href="{{ $media->link() }}" class="gather-small-media" title="{{ $media->name }}">
    @if(auth()->check())
        @if(auth()->user()->name == $media->user->name)
            <button data-media="{{ $media->id }}" class="gather-delete-media"></button>
            <button data-media="{{ $media->id }}" class="gather-edit-media"></button>
        @endif
    @endif

    <span class="gather-small-media-duration">{{ $media->time() }}</span>
    <img src="{{ $media->get_media_thumbnail() }}"
         alt="Miniature de {{ $media->name }}}" class="gather-small-media-preview">
    <div class="gather-media-infos">
        <h2>{{ $media->get_small_title() }}</h2>
        <span>{{ $media->posted() }}</span>
    </div>
    <div class="gather-media-infos-user">
        <div>
            <img src="{{ $media->user->avatar() }}" alt="Photo de profil de {{ $media->user->name }}">
        </div>
        <span>{{ $media->user->name }}</span>
    </div>
</a>
