<div class="gather-main-media">
    <div class="gather-main-media-wrapper">
        <video controls preload="auto" id="gather_main_media" src="{{ $media->get_media_url() }}" type="video/mp4"></video>
    </div>
    <div class="gather-media-infos">
        <div class="gather-media-infos-part">
            <div>
                <p class="gather-media-title">{{ $media->name }}</p>
                <span>{{ $media->posted() }} | {{ $media->views() }}</span>
                <p>{{ $media->description }}</p>
            </div>

            <div class="gather-media-author">
                <div>
                    <span>{{ $media->user->name }}</span>
                    <x-buttons.follow :user="$media->user"></x-buttons.follow>
                </div>
                <a href="{{ $media->user->profil() }}" class="gather-media-author-img-wrapper">
                    <img src="{{ $media->user->avatar() }}" alt="Photo de profil de {{ $media->user->name }}">
                </a>
            </div>
        </div>
    </div>
</div>
