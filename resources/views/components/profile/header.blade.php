<div class="gather-profile-header">
    <div class="gather-profile-header-user">
        <div class="gather-profile-image-wrapper">
            <div>
                <a href="/user/edit" class="gather-user-edit-btn"></a>
                <img src="{{ $user->avatar() }}" alt="photo de profil de {{ $user->name }}">
            </div>
        </div>
        <h1> {{'@'.$user->slug }}</h1>
        <x-profile.followers :user="$user"></x-profile.followers>
    </div>
    <x-buttons.follow :user="$user"></x-buttons.follow>
</div>

