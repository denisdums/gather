@if(auth()->check())
    @if($user->id != auth()->user()->id)
        <form action="/relationship" method="post" id="gather-relationship-form">
            @csrf
            <input type="hidden" name="user_id" id="follow_user_id" value="{{ $user->id }}">
        </form>
        @if(! $user->following())
            <button class="gather-follow-button gather-button gather-button-green">Suivre</button>
        @else
            <button class="gather-follow-button gather-button gather-button-black">Ne plus suivre</button>
        @endif
    @endif
@else
    <a href="/login" class="gather-button gather-button-green">Suivre</a>
@endif
