@auth
    <a href="/user/{{ auth()->user()->slug }}" class="gather-user-connected"
       title="{{ auth()->user()->name }}">
        <div class="gather-user-connected-image-wrapper">
            <img src="{{ auth()->user()->avatar() }}" alt="{{ auth()->user()->name }}">
        </div>
        <span>{{ auth()->user()->name }}</span>
    </a>
@endauth
