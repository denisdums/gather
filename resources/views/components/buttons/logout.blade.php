<button class="gather-logout-button gather-menu-item gather-menu-item-new-part" title="Déconnexion">
    <div class="gather-menu-item-wrapper-icon">
        <img src="{{ asset('img/icons/fi-br-sign-out.svg') }}" alt="icon de deconnexion">
    </div>
    <span>Se déconnecter</span>
</button>
<form id="logout-form" action="{{ route('logout') }}" method="POST">
    @csrf
</form>
