<a href="{{ $route }}" class="gather-menu-item" title="{{ $title }}">
    <div class="gather-menu-item-wrapper-icon">
        <img src="{{ asset("img/icons/fi-br-$icon.svg") }}">
    </div>
    <span>{{ $title }}</span>
</a>
