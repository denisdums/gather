@extends('layouts.app')
@section('title','Home')
@section('content')
    <div class="container gather-container-404">
        <h1 class="gather-404-title">Oh cette page est introuvable 😞</h1>
        <a href="/" class="gather-button gather-button-black">Retour à l'accueil</a>
    </div>
@endsection
