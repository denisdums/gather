@extends('layouts.app')

@section('content')
    <div class="container">
    <form method="POST" action="{{ route('login') }}" class="gather-form">
        @csrf
        <div class="gather-form-input-wrapper">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <label for="password">Mot de passe</label>
            <input id="password" type="password" name="password" required autocomplete="current-password">
            @error('password')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <p>Se souvenir de moi</p>
            <input class="form-check-input gather-form-input-wrapper-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label for="remember"></label>
        </div>

        <button type="submit" class="gather-form-submit">Se connecter</button>

        {{--@if (Route::has('password.request'))
            <a href="{{ route('password.request') }}" class="gather-form-reset">
                Reset Password
            </a>
        @endif--}}
    </form>
    </div>

@endsection
