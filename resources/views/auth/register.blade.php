@extends('layouts.app')

@section('content')
    <div class="container">
    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" class="gather-form">
        @csrf

        <div class="gather-form-input-wrapper">
            <label for="name">Nom</label>
            <input id="name" type="text" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <p>Avatar</p>
            <input id="avatar" type="file" name="avatar" required class="gather-form-input-file">
            <label for="avatar" class="gather-file-input-label"></label>
            @error('avatar')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <label for="password">Mot de passe</label>
            <input id="password" type="password" name="password" required autocomplete="new-password">
            @error('password')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <label for="password_confirmation">Mot de passe (confirmation)</label>
            <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
            @error('password_confirmation')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="gather-form-submit">S'enregistrer</button>

    </form>
    </div>

@endsection
