@extends('layouts.app')
@section('title', 'Abonnements')
@section('content')
    <div class="container">
        @if(isset($subs) && count($subs) > 0)
            <div class="gather-small-medias">
                @foreach($subs as $sub)
                    <x-medias.small :media="$sub"></x-medias.small>
                @endforeach
            </div>
        @else
            <p>Pas de vidéos dans les abonnements 😞</p>
        @endif
    </div>
@endsection

