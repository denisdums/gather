@extends('layouts.app')
@section('title', $user->name)
@section('content')

    <div class="container">
        <x-profile.header :user="$user"></x-profile.header>

        @if($user_medias)
            <div class="gather-small-medias">
                @foreach($user_medias as $user_media)
                    <x-medias.small :media="$user_media"></x-medias.small>
                @endforeach
            </div>
        @else
            <p>Pas de vidéos pour cet utilisateur 😞</p>
        @endif

    </div>

@endsection

