@extends('layouts.app')

@section('content')
    <div class="container">
        <x-profile.header :user="$user"></x-profile.header>

    <form method="POST" enctype="multipart/form-data" class="gather-form">
        @csrf

        <div class="gather-form-input-wrapper">
            <label for="name">Nom</label>
            <input id="name" type="text" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
            @error('name')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{ $user->email  }}" required autocomplete="email">
            @error('email')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <p>Changer d'avatar</p>
            <input id="avatar" type="file" name="avatar" class="gather-form-input-file">
            <label for="avatar" class="gather-file-input-label"></label>
            @error('avatar')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <button type="submit" class="gather-form-submit">Editer</button>

        </form>
    </div>

@endsection
