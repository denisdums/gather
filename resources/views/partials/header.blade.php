<header>
    <div class="container">

        <a href="/" class="gather-logo" title="Accueil">
            <img src="{{ asset('img/gather-logo.svg') }}" alt="Logo du site Gather">
        </a>

        <div class="gather-search">
            <form>
                @csrf
                <input type="search" id="gather-search-bar" autocomplete="off">
            </form>
            <div class="gather-search-results"></div>
        </div>


        <div>
            <input type="checkbox" id="gather-burger-checkbox">
            <label for="gather-burger-checkbox">
                <span></span>
            </label>

            <div class="gather-menu">
                <div class="gather-menu-top"></div>
                <div class="gather-menu-items">
                    <x-buttons.userconnected></x-buttons.userconnected>
                    <x-buttons.menu route="/" title="Accueil" icon="home"></x-buttons.menu>
                    @guest
                        <x-buttons.menu route="/login" title="Se connecter" icon="key"></x-buttons.menu>
                        <x-buttons.menu route="/register" title="S'enregistrer" icon="user-add"></x-buttons.menu>
                    @else
                        <x-buttons.menu route="/subscriptions" title="Abonnements" icon="play-alt"></x-buttons.menu>
                        <hr class="gather-item-separator">
                        <x-buttons.menu route="/{{ auth()->user()->slug }}/add" title="Ajouter une vidéo" icon="file-add"></x-buttons.menu>
                        <hr class="gather-item-separator">
                        <x-buttons.logout></x-buttons.logout>
                    @endguest
                </div>
            </div>
        </div>
    </div>
</header>
