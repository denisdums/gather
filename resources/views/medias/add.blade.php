@extends('layouts.app')
@section('title','Add')
@section('content')
    <div class="container">
    <form method="post" class="gather-form" enctype="multipart/form-data">
        @csrf

        <div class="gather-form-input-wrapper">
            <label for="media-name">Nom du média</label>
            <input type="text" name="media-name" id="media-name" value="{{ old('media-name') }}" required autofocus>
            @error('media-name')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <label for="media-description">Description du média</label>
            <textarea name="media-description" id="media-description" cols="30" rows="10"></textarea>
            @error('media-description')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <div class="gather-form-input-wrapper">
            <p>Média</p>
            <input id="media" type="file" name="media-file" accept="video/*" required class="gather-form-input-file">
            <label for="media" class="gather-form-media-file-label"></label>
            @error('media')
            <p class="gather-form-error">{{ $message }}</p>
            @enderror
        </div>

        <input type="submit" class="gather-form-submit">
    </form>
    </div>
@endsection

