@extends('layouts.app')
@section('title', $main_media->name)
@section('content')

    <div class="container">
        @if($main_media)
            <x-medias.tall :media="$main_media"></x-medias.tall>
            @if(count($other_medias) > 0)
                <h3 class="gather-next-medias-title">Autres vidéos de {{ $main_media->user->name }}</h3>
                <div class="gather-small-medias">
                    @foreach($other_medias as $other_media)
                        <x-medias.small :media="$other_media"></x-medias.small>
                    @endforeach
                </div>
            @else
                <h3 class="gather-next-medias-title">{{ $main_media->user->name }} n'a pas d'autres vidéos 😞</h3>
            @endif
        @else
            <p>Pas de vidéos pour cet utilisateur 😞</p>
        @endif

    </div>

@endsection

