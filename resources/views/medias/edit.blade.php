@extends('layouts.app')
@section('title','Add')
@section('content')
    <div class="gather-edit-media-wrapper container">
        <div class="gather-edit-media-resume">
            <span class="gather-small-media-duration">{{ $media->time() }}</span>
            <img src="{{ $media->get_media_thumbnail() }}" alt="">
        </div>
        <form method="post" class="gather-form" enctype="multipart/form-data">
            @csrf
            <div class="gather-form-input-wrapper">
                <label for="media-name">Nom du média</label>
                <input type="text" name="media-name" id="media-name" value="{{ $media->name }}" required autofocus>
                @error('media-name')
                <p class="gather-form-error">{{ $message }}</p>
                @enderror
            </div>

            <div class="gather-form-input-wrapper">
                <label for="media-description">Description du média</label>
                <textarea name="media-description" id="media-description" cols="30" rows="10">{{ $media->description }}</textarea>
                @error('media-description')
                <p class="gather-form-error">{{ $message }}</p>
                @enderror
            </div>

            <input type="submit" class="gather-form-submit" value="Editer">
        </form>
    </div>

@endsection

