/** Follow **/
let follow_button = $('.gather-follow-button');
follow_button.on('click', function (e) {
    e.preventDefault();
    let id = $('#follow_user_id').val();
    var data = {'user_id': id};
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: "POST",
        data: data,
        url: '/relationship',
        success: function (response) {
            let result = JSON.parse(response);
            render_follow_button(result.following)
            render_follower_count(result.followers)
        }
    });
});

function render_follow_button(isfollowing){
    let follow_button = $('.gather-follow-button');
    if (isfollowing == 1){
        follow_button.removeClass('gather-button-green');
        follow_button.addClass('gather-button-black');
        follow_button.html('Ne plus suivre');
    }else{
        follow_button.addClass('gather-button-green');
        follow_button.removeClass('gather-button-black');
        follow_button.html('Suivre');
    }
}

function render_follower_count(countfollowers){
    let followers = $('.gather-followers');
    if (countfollowers > 1){
        followers.html( countfollowers + ' abonnés')
    } else {
        followers.html( countfollowers + ' abonné')
    }
}

/** Logout **/
let logout_button = $('.gather-logout-button');
logout_button.on('click', function (e) {
    e.preventDefault();
    $('#logout-form').submit();
})

/** File Field **/
let file_field = $('.gather-form-input-file');
if (file_field.length) {
    file_field.on('change', function () {
        input_preview(this)
    })
}

function input_preview(input) {
    if (input.files && input.files[0]) {

        if ($('.gather-file-input-label').length){
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.gather-file-input-label').css('background-image', 'url("' + e.target.result + '")');
                $('.gather-file-input-label').css('background-size', 'auto 80%');
            }
            reader.readAsDataURL(input.files[0]);
        }
        if ($('.gather-form-media-file-label').length){
            $('.gather-form-media-file-label').css('background-image', 'url("/img/file-validate.svg")');
            $('.gather-form-media-file-label').prepend('<p class="gather-form-media-file-label-infos">'+input.files[0].name+'</p>');
        }

    }
}


$('#gather-search-bar').on('paste keyup search', function (){
    let val = $(this).val();
    $('.gather-search-results').html('');
    if (val.length > 0){
    var data = {'search': val};
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            data: data,
            url: '/search',
            success: function (response) {
                let results = JSON.parse(response);

                $('.gather-search-results').html('');
                results.forEach(function (result){
                    console.log(result);
                    $('.gather-search-results').append('<a href="/watch/'+result.slug+'">'+result.name+'</a>')
                });
            }
        });
    }
});


$('.gather-search-results a').on('click', function (){
    console.log($(this))
})


/** Delete Media **/

let delete_button =  $('.gather-delete-media');

delete_button.on('click', function (e){
    e.preventDefault();
    let id = $(this).attr('data-media');
    var data = {'media_id': id};
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: "POST",
        data: data,
        url: '/media-delete',
        success: function (response) {
            document.location.reload();
        }
    });
})

/** Edit Media **/

let edit_button =  $('.gather-edit-media');

edit_button.on('click', function (e){
    e.preventDefault();
    let id = $(this).attr('data-media');
    document.location.href="/edit/media/"+id;
})
